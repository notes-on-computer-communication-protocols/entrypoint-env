(
set -o errexit -o nounset -o noglob
( set -o pipefail 2> /dev/null
) \
&& set -o pipefail \
|| true
command -v shopt > /dev/null && shopt -s failglob
set -x
time apt-get update --quiet=2
if ! time apt-get install \
        --no-install-suggests \
        --no-install-recommends \
        --quiet=2 \
    psmisc \
    tini \
    > /tmp/apt 2>&1
then cat /tmp/apt ; false
fi
)