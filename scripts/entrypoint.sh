set -o errexit -o nounset -o noglob
( set -o pipefail 2> /dev/null
) \
&& set -o pipefail \
|| true
command -v shopt > /dev/null && shopt -s failglob
(
    echo $CI_JOB_IMAGE
    . /etc/os-release
    echo $PRETTY_NAME ${VERSION_ID:-}
)

export TIME=$(printf '\n\e[90;47m')%e$(printf '\e[0m seconds')
export TIMEFORMAT=$(printf '\n\e[90;47m')%R$(printf '\e[0m seconds')

(
case $(. /etc/os-release ; echo $ID) in
    alpine|wolfi)
        set -x
        time apk add \
                --update-cache \
                --quiet \
            tini \
            ;
        ;;
    debian|ubuntu)
        bash $CI_PROJECT_DIR/scripts/apt/entrypoint.sh
        #^ bash for builtin time command
        ;;
    fedora)
        case $CI_JOB_IMAGE in
            fedora \
            |quay.io/fedora/fedora)
                export DNF=dnf
                ;;
            quay.io/fedora/fedora-minimal)
                export DNF=microdnf
                ;;
            *)
                echo ERROR: Unknown image
                exit 2
                ;;
        esac
        set -x
        time $DNF install \
                --assumeyes \
                --quiet \
                --setopt=install_weak_deps=False \
            psmisc \
            tini \
            ;
        ;;
    *)
        echo ERROR: Unknown distribution
        exit 1
        ;;
esac
)

exec tini -- env "$@"
