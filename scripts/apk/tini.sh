set -o errexit -o nounset -o noglob -o pipefail
command -v shopt > /dev/null && shopt -s failglob
(
    . /etc/os-release
    echo $PRETTY_NAME
    echo $VERSION_ID
)

apk add --update-cache --quiet \
    tini \
    ;

exec tini -- env "$@"
