set -o errexit -o nounset -o noglob
( set -o pipefail 2> /dev/null
) \
&& set -o pipefail \
|| true
command -v shopt > /dev/null && shopt -s failglob
(. /etc/os-release ; echo $PRETTY_NAME)

echo "$@"
exec env "$@"